#Set Domain Controller audit policies
Auditpol /set /subcategory:"Credential Validation" /Success:enable /failure:enable
Auditpol /set /subcategory:"Other Account Logon Events" /Success:enable /failure:enable
Auditpol /set /subcategory:"Kerberos Authentication Service" /Success:enable /failure:enable
Auditpol /set /subcategory:"Kerberos Service Ticket Operations" /Success:enable /failure:enable
Auditpol /set /subcategory:"Computer Account Management" /Success:enable /failure:enable
Auditpol /set /subcategory:"Distribution Group Management" /Success:enable /failure:enable
Auditpol /set /subcategory:"Other Account Management" /Success:enable /failure:enable
Auditpol /set /subcategory:"Security Group Management" /Success:enable /failure:enable
Auditpol /set /subcategory:"User Account Management" /Success:enable /failure:enable
Auditpol /set /subcategory:"DPAPI Activity" /Success:enable /failure:enable
Auditpol /set /subcategory:"Plug and Play Events" /Success:enable /failure:enable
Auditpol /set /subcategory:"Process Creation" /Success:enable /failure:enable
Auditpol /set /subcategory:"Process Termination" /Success:enable /failure:enable
Auditpol /set /subcategory:"Directory Service Access" /Success:enable /failure:enable
Auditpol /set /subcategory:"Directory Service Changes" /Success:enable /failure:enable
Auditpol /set /subcategory:"Directory Service Replication" /Success:enable /failure:enable
Auditpol /set /subcategory:"Account Lockout" /Success:enable /failure:enable
Auditpol /set /subcategory:"User / Device Claims" /Success:enable /failure:enable
Auditpol /set /subcategory:"Group Membership" /Success:enable /failure:enable
Auditpol /set /subcategory:"Logoff" /Success:enable /failure:enable
Auditpol /set /subcategory:"Logon" /Success:enable /failure:enable
Auditpol /set /subcategory:"Other Logon/Logoff Events" /Success:enable /failure:enable
Auditpol /set /subcategory:"Special Logon" /Success:enable /failure:enable
Auditpol /set /subcategory:"Detailed File Share" /Success:enable
Auditpol /set /subcategory:"File Share" /Success:enable /failure:enable
Auditpol /set /subcategory:"File System" /Success:enable /failure:enable
Auditpol /set /subcategory:"Filtering Platform Connection" /failure:enable
Auditpol /set /subcategory:"Other Object Access Events" /Success:enable /failure:enable
Auditpol /set /subcategory:"Registry" /Success:enable /failure:enable
Auditpol /set /subcategory:"Removable Storage" /Success:enable /failure:enable
Auditpol /set /subcategory:"Audit Policy Change" /Success:enable /failure:enable
Auditpol /set /subcategory:"Authentication Policy Change" /Success:enable /failure:enable
Auditpol /set /subcategory:"MPSSVC Rule-Level Policy Change" /Success:enable /failure:enable
Auditpol /set /subcategory:"Other Policy Change Events" /Success:enable /failure:enable
Auditpol /set /subcategory:"Non Sensitive Privilege Use" /failure:enable
Auditpol /set /subcategory:"Sensitive Privilege Use" /Success:enable /failure:enable
Auditpol /set /subcategory:"Other System Events" /Success:enable /failure:enable
Auditpol /set /subcategory:"Security State Change" /Success:enable /failure:enable
Auditpol /set /subcategory:"Security System Extension" /Success:enable /failure:enable
Auditpol /set /subcategory:"System Integrity" /Success:enable /failure:enable

#Enable include commandline in process creation log
New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System\Audit" -Name "ProcessCreationIncludeCmdLine_Enabled" -Value 1  -PropertyType "DWORD"

#Enable Powershell module logging
New-Item –Path "HKLM:\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows" –Name PowerShell
New-Item –Path "HKLM:\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\PowerShell" –Name ModuleLogging
New-Item –Path "HKLM:\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\PowerShell\ModuleLogging" –Name ModuleNames
New-ItemProperty -Path "HKLM:\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\PowerShell\ModuleLogging" -Name "EnableModuleLogging" -Value 1  -PropertyType "DWORD"
New-ItemProperty -Path "HKLM:\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\PowerShell\ModuleLogging\ModuleNames" -Name "*" -Value "*" -PropertyType "String"

#Optionally enable Powershell script block logging
#New-Item –Path "HKLM:\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\PowerShell" –Name ScriptBlockLogging
#New-ItemProperty -Path "HKLM:\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\PowerShell\ScriptBlockLogging" -Name "EnableScriptBlockLogging" -Value 1  -PropertyType "DWORD"

gpupdate /force